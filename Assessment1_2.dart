void main() {

  var winningApps = new Map();
  winningApps[2012] = "FNB Banking";
  winningApps[2013] = "SnapScan";
  winningApps[2014] = "Live Inspect";
  winningApps[2015] = "WumDrop";
  winningApps[2016] = "Domestly";
  winningApps[2017] = "Standard Bank Shyft";
  winningApps[2018] = "Khula";
  winningApps[2019] = "Naked Insurance";
  winningApps[2020] = "EasyEquities";
  winningApps[2021] = "Ambani";

  print("Winning apps of the MTN Business App of the Year Awards since 2012");
  print("$winningApps \n");

  var sortedKeys = winningApps.values.toList()..sort();
  print("The apps sorted by name:");
  print("$sortedKeys \n");

  var app2017 = winningApps[2017];
  print("The winning app for 2017 was: $app2017");

  var app2018 = winningApps[2017];
  print("The winning app for 2018 was: $app2018 \n");

  var totalNumberOfApps = winningApps.length;
  print("Total number of apps: $totalNumberOfApps");

}


