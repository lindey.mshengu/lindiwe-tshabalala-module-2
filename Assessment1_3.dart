class Main {

  var categories = [
    "Public sector", 
    "Private sector",
    "Private sector",
    "Public sector",
    "Public sector",
    "Public sector",
    "Private sector",
    "Private sector",
    "Public sector",
    "Private sector"];

  var developers = [
    "Fredrick Kobo",
    "Kobus Ehhlers",
    "Jasper Van Heesch",
    "Simon Hartley",
    "Thato Marumo",
    "Arno Van Helden",
    "Mathew Piper",
    "Alex Thompson",
    "Charles Savage",
    "Mukundi Lambani"];

  var years = [
    2012, 
    2013, 
    2014, 
    2015, 
    2016, 
    2017, 
    2018, 
    2019, 
    2020, 
    2021];

  List toUpperCase(List winningApps) {
    var apps = [];
    for (var i=0; i<winningApps.length; i++) {
      apps.add(winningApps[i].toString().toUpperCase());
      
    } return apps;
    
  }
}

void main() {
    
  var winningApps = [
  "FNB Banking", 
  "SnapScan", 
  "Live Inspect", 
  "WumDrop", 
  "Domestly", 
  "Standard Bank Shyft", 
  "Khula", 
  "Naked Insurance",
  "EasyEquities", 
  "Ambani"];

  Main myObject = new Main();

  var appNames = myObject.toUpperCase(winningApps);
  var sectors = myObject.categories;
  var developers = myObject.developers;
  var years = myObject.years;

  print("MTN Business App of the Year Awards");
  for (var j = 0; j<myObject.toUpperCase(winningApps).length; j++) {
    var app = appNames[j];
    var sector = sectors[j];
    var dev = developers[j];
    var year = years[j];

    print("AppName: $app | Sector: $sector | Developer: $dev | Year: $year");

  }
  
}
